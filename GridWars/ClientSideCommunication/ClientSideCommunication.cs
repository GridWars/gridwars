﻿
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;

public class ClientSideCommunication
{
    public static void Main(String[] args)
    {
        bool HaveNotHitServerYet = true;
        IPAddress ip_address = IPAddress.Parse("54.166.205.17"); //default
        int port = 5001;
        try
        {
            Console.WriteLine("Attempting to connect to server at IP address: {0} port: {1}",
                                                ip_address.ToString(), port);
            TcpClient client = new TcpClient(ip_address.ToString(), port);
            Console.WriteLine("Connection successful!");
            StreamReader reader = new StreamReader(client.GetStream());
            StreamWriter writer = new StreamWriter(client.GetStream());
            string stringReadFromServer = string.Empty;
            string toSendToTheServer = string.Empty;
            string usernameOfConnectedPlayer = args[0];
            if (HaveNotHitServerYet)
            {
                HaveNotHitServerYet = false;
                toSendToTheServer = usernameOfConnectedPlayer + "|" + usernameOfConnectedPlayer + "|";
            }

            while (!stringReadFromServer.Equals("Exit"))
            {
                writer.WriteLine(toSendToTheServer);
                writer.Flush();
                stringReadFromServer = reader.ReadLine();
                if (stringReadFromServer.Contains("You have been invited by "))
                {
                    DialogResult resultFromInvitation = MessageBox.Show(stringReadFromServer + "", "Invitation", MessageBoxButtons.YesNo);
                }
            }
            reader.Close();
            writer.Close();
            client.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    } // end Main()
} // end class definition
