﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using dataconn;

namespace GridWars
{
    public partial class PasswordChangeWindow : Form
    {

        public string Username = "";
        private dbconn _connect;
        public string[,] PlayerInformation;

        public PasswordChangeWindow(string username, string password)
        {
            InitializeComponent();
            Username = username;
            _connect = new dbconn(username, password);
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {
            if(oldPassword.Text == "" || newPassword.Text == "" || newPassword2.Text == "")
            {
                MessageBox.Show("Please enter values for all three fields and try again!");
            }
            else if(newPassword.Text == oldPassword.Text || newPassword2.Text == oldPassword.Text)
            {
                MessageBox.Show("Your new password cannot be the same as your old one!");
            }
            else if(newPassword.Text == newPassword2.Text)
            {
                //change pwd logic
                MessageBox.Show("Your password has been changed!");
                this.Hide();
            }
            else
            {
                MessageBox.Show("Your passwords do not match, or they do not meet the password requirments. Please try again.");
            } 
        }
    }
}
