﻿namespace GridWars
{
    partial class AdminDeleteWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose ( );
            }
            base.Dispose (disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ( )
        {
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.noSelection = new System.Windows.Forms.Label();
            this.userDisplay = new System.Windows.Forms.Label();
            this.DeletePlayerLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(328, 375);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(113, 46);
            this.button1.TabIndex = 0;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(63, 138);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(161, 264);
            this.listBox1.TabIndex = 1;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // noSelection
            // 
            this.noSelection.AutoSize = true;
            this.noSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noSelection.Location = new System.Drawing.Point(324, 114);
            this.noSelection.Name = "noSelection";
            this.noSelection.Size = new System.Drawing.Size(128, 40);
            this.noSelection.TabIndex = 2;
            this.noSelection.Text = "        Error: \r\nNo user selected";
            // 
            // userDisplay
            // 
            this.userDisplay.AutoSize = true;
            this.userDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userDisplay.Location = new System.Drawing.Point(331, 218);
            this.userDisplay.Name = "userDisplay";
            this.userDisplay.Size = new System.Drawing.Size(110, 26);
            this.userDisplay.TabIndex = 3;
            this.userDisplay.Text = "              ";
            // 
            // DeletePlayerLabel
            // 
            this.DeletePlayerLabel.AutoSize = true;
            this.DeletePlayerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeletePlayerLabel.Location = new System.Drawing.Point(106, 9);
            this.DeletePlayerLabel.Name = "DeletePlayerLabel";
            this.DeletePlayerLabel.Size = new System.Drawing.Size(319, 31);
            this.DeletePlayerLabel.TabIndex = 4;
            this.DeletePlayerLabel.Text = "Delete Players Account";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(109, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(315, 34);
            this.label1.TabIndex = 5;
            this.label1.Text = "Below is a list of all users. Please select a user\'s \r\naccount and hit delete to " +
    "delete their account.";
            // 
            // AdminDeleteWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 490);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DeletePlayerLabel);
            this.Controls.Add(this.userDisplay);
            this.Controls.Add(this.noSelection);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button1);
            this.Name = "AdminDeleteWindow";
            this.Text = "AdminDelete";
            this.Load += new System.EventHandler(this.AdminDeleteWindow_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label noSelection;
        private System.Windows.Forms.Label userDisplay;
        private System.Windows.Forms.Label DeletePlayerLabel;
        private System.Windows.Forms.Label label1;
    }
}