﻿using System;
using user_space;
using Npgsql;
using System.Collections.Generic;

namespace dataconn
{
    /// <summary>
    /// 
    /// </summary>
    public class dbconn
    {

        NpgsqlConnection _conn;

        public dbconn(string name, string password)
        {
            _conn = new NpgsqlConnection("Server=54.166.205.17;User Id=" + name + ";Password=" + password + ";Database=gridwars;");
            _conn.Open();
        }

        public bool login(string name)
        {
            // Define a query returning a single row result set
            NpgsqlCommand _command = new NpgsqlCommand("UPDATE users SET online = 'y' WHERE screen_name = '" + name + "'", _conn);
            _command.ExecuteNonQuery();

            return true;
        }

        public void logOff(string name)
        {
            NpgsqlCommand _command = new NpgsqlCommand("UPDATE users SET online = 'n' WHERE screen_name = '" + name + "'", _conn);
            _command.ExecuteNonQuery();

            _conn.Close();
        }

        public void delete(string name)
        {
            // Define a query returning a single row result set
            NpgsqlCommand _command = new NpgsqlCommand("DELETE FROM users WHERE screen_name = '" + name + "'", _conn);

            _command.ExecuteNonQuery();
        }

        public void create(User _user)
        {
            _conn = new NpgsqlConnection("Server=54.166.205.17;User Id=Create;Password=NrZ:K4z3mj;Database=gridwars;");
            _conn.Open();

            // Define a query returning a single row result set
            NpgsqlCommand _command = new NpgsqlCommand("INSERT INTO users VALUE('" + _user.screen_name + "', '" + _user.user_name + "', '" +
                _user.email + "', '" + _user.password + "', 'n', '1990-01-08', '1990-01-08', 'n', 'n', 'n', 'n', 'n'", _conn);

            _command.ExecuteNonQuery();

            _conn.Close();
        }

        public string[,] checkOnline(string name)
        {
            List<string> friends = new List<string>();
            List<string> users = new List<string>();
            List<string> online = new List<string>();
            List<string> in_game = new List<string>();
            string[,] usersToReturn;

            NpgsqlCommand _command = new NpgsqlCommand("SELECT user_2 FROM friends WHERE user_1 = '" + name + "'", _conn);

            NpgsqlDataReader _read = _command.ExecuteReader();


            while(_read.Read())
            {
                friends.Add((string)_read[0]);
            }

            _command.Cancel();
            _command = new NpgsqlCommand("SELECT screen_name, online, in_game FROM users", _conn);

            _read.Close();
            _read = _command.ExecuteReader();


            while(_read.Read())
            {
                users.Add((string)_read[0]);
                string busy = ((bool)_read[1] == true) ? "y": "n";
                in_game.Add(busy);
                string isOnline = ((bool)_read[2] == true) ? "y" : "n";
                online.Add(isOnline);
            }

            usersToReturn = new string[online.Count, 4];

            for (int i = 0; i < online.Count; i++)
            { 

                usersToReturn[i, 0] = users[i];
                usersToReturn[i, 1] = online[i];
                usersToReturn[i, 2] = in_game[i];

                if (friends.Contains(users[i]))
                {
                    usersToReturn[i, 3] = "y";
                }
                else
                {
                    usersToReturn[i, 3] = "n";
                }
            }

            return usersToReturn;
        }

        //method to check if the email for the account has been verfied.

        public Boolean verifyEmail(string name)
        {
            NpgsqlCommand _command = new NpgsqlCommand("Select verified from users WHERE screen_name = '" + name + "'", _conn);
            Boolean verified = (bool)_command.ExecuteScalar();

            if (verified == true)
            {
                return verified;
            }
            else
            {
                return false;
            }

        }

        

    }
}