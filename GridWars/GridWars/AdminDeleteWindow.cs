﻿using System;
using dataconn;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GridWars
{
    public partial class AdminDeleteWindow : Form
    {
        public string Username = "";
        private dbconn _connect;
        public string[,] PlayerInformation;

        public AdminDeleteWindow (string username,string password)
        {
            InitializeComponent ( );
            Username = username;
            _connect = new dbconn (username, password);

            updateUserList (username);
            
        }

        public void updateUserList (string username)
        {
            PlayerInformation = _connect.checkOnline (username);

            for (int i = 0; i < (PlayerInformation.Length/4); i++)
            {
                    listBox1.Items.Add (PlayerInformation[i,0]);
              
            }
            
        }



        private void label4_Click (object sender, EventArgs e)
        {

        }

        private void AdminDeleteWindow_Load (object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged (object sender, EventArgs e)
        {
            noSelection.Text = "User selected:";
            try
            {
                userDisplay.Text = listBox1.SelectedItem.ToString ( );
            }
            catch (Exception ex)
            {
                userDisplay.Text = "";
            }
        }

        private void button1_Click_1 (object sender, EventArgs e)
        {
            try
            {
                listBox1.SelectedItem.ToString ( );          //test if a user was selected
            }
            catch (System.NullReferenceException n)
            {

                return;
            }

            string acceptBan = "Are you sure you wish to delete " + listBox1.SelectedItem.ToString ( );
            string title = "Delete?";
            DialogResult result = MessageBox.Show (this, acceptBan, title, MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                //insert code to delete
                string success = listBox1.SelectedItem.ToString ( ) + " has been successfully deleted.";
                MessageBox.Show (this, success, title, MessageBoxButtons.OK);
                Close ( );
            }
            else {
                listBox1.ClearSelected ( );
            }
        }

        private void AdminDeleteWindow_Load_1(object sender, EventArgs e)
        {

        }
    }
}
