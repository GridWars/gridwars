﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace user_space
{
    public class User
    {
        public string screen_name { get; set; }
        public string user_name { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        User(string sName, string uName, string mail, string pass)
        {
            screen_name = sName;
            user_name = uName;
            email = mail;
            password = pass;
        }
    }
}
