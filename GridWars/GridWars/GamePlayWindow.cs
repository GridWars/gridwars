﻿using dataconn;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GridWars
{
    public partial class GamePlayWindow : Form
    {
        public string SelectedInvitee = "";
        public string Username = "";
        public string[,] PlayerInformation;
        public string OnlineStatusOfInvitedPlayer;
        public string InGameStatusOfInvitedPlayer;
        public string UsernameOfInvitedPlayer;
        public bool InvitedPlayerIsAFRiendOfCurrentPlayer;
        private string Password;
        private dbconn _connect;

        public GamePlayWindow(string username, string password)
        {
            InitializeComponent();
            Username = username;
            _connect = new dbconn(username, password);
            UpdateFriendAndOthersList(username);
            Password = password;
        }

        private void FriendsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (OthersList.SelectedItems.Count != 0)
            {
                //work around so indexed change event doesn't occur 
                OthersList.SelectedIndexChanged -= OthersList_SelectedIndexChanged;
                OthersList.ClearSelected();
                OthersList.SelectedIndexChanged += OthersList_SelectedIndexChanged;
            }
            SelectedInvitee = FriendsList.SelectedItem.ToString();

            string[] splitPartsOSelectedIndex = SelectedInvitee.Split('(');
            string usernameOfInvited = splitPartsOSelectedIndex[0];

            UsernameOfInvitedPlayer = usernameOfInvited;

            string statusOnline = splitPartsOSelectedIndex[1].Split(',')[0];
            OnlineStatusOfInvitedPlayer = statusOnline;
            string InGameToPlay = "";

            try
            {
                InGameToPlay = splitPartsOSelectedIndex[1].Split(',')[1];
                InGameStatusOfInvitedPlayer = InGameToPlay.Remove(InGameToPlay.Length - 1, 1);
            }
            catch (Exception)
            {

                InGameStatusOfInvitedPlayer = "InGame";
            }


            InvitedPlayerIsAFRiendOfCurrentPlayer = true;

        }

        private void OthersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (FriendsList.SelectedItems.Count != 0)
            {
                FriendsList.SelectedIndexChanged -= FriendsList_SelectedIndexChanged;
                FriendsList.ClearSelected();
                FriendsList.SelectedIndexChanged += FriendsList_SelectedIndexChanged;
            }
            SelectedInvitee = OthersList.SelectedItem.ToString();

            string[] splitPartsOSelectedIndex = SelectedInvitee.Split('(');
            string usernameOfInvited = splitPartsOSelectedIndex[0];

            UsernameOfInvitedPlayer = usernameOfInvited;

            string statusOnline = splitPartsOSelectedIndex[1].Split(',')[0];
            OnlineStatusOfInvitedPlayer = statusOnline;

            string InGameToPlay = splitPartsOSelectedIndex[1].Split(',')[1];
            InGameStatusOfInvitedPlayer = InGameToPlay;

            InGameStatusOfInvitedPlayer = InGameToPlay.Remove(InGameToPlay.Length - 1, 1);
            InvitedPlayerIsAFRiendOfCurrentPlayer = false;

        }

        private void InviteButton_Click(object sender, EventArgs e)
        {
            if (FriendsList.SelectedItems.Count == 0 && OthersList.SelectedItems.Count == 0)
            {
                MessageBox.Show(Username + ", please select a person to invite from the FRiends list or Others list!", "Grid War Message");
            }
            else
            {
                if (InvitedPlayerIsAFRiendOfCurrentPlayer)
                {
                    if (InGameStatusOfInvitedPlayer == "NotBusy" && OnlineStatusOfInvitedPlayer == "Online")
                    {
                        MessageBox.Show(UsernameOfInvitedPlayer + " has been sent an invitation!", "Grid War Message");
                        //send invitation
                        //client-server logic
                    }
                    else
                    {
                        MessageBox.Show(UsernameOfInvitedPlayer + " is not available to play at the moment. Try again some other time.", "Grid War Message");
                    }
                }
                else
                {
                    if (InGameStatusOfInvitedPlayer == "NotBusy")
                    {
                        MessageBox.Show(UsernameOfInvitedPlayer + " has been sent an invitation!", "Grid War Message");
                        //send invitation
                        //client-server logic
                    }
                    else
                    {
                        MessageBox.Show(UsernameOfInvitedPlayer + " is not available to play at the moment. Try again some other time.", "Grid War Message");
                    }
                }
            }
        }

        private void changePasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PasswordChangeWindow changePwd = new PasswordChangeWindow(Username, Password);
            changePwd.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GamePlayWindow_Load(object sender, EventArgs e)
        {

        }
        private void ContactServerAboutInvite()
        {
            //TcpClient client = new TcpClient("54.166.205.17", 5001);
            //StreamReader reader = new StreamReader(client.GetStream());
            //StreamWriter writer = new StreamWriter(client.GetStream());

            //writer.WriteLine("Invite|" + CurrentPlayerUsername "|" + SelectedInvitee);
            //writer.Flush();
            //client.Close();
        }
        public void FillFriendsList()
        {
            string yes = "y";
            for (int i = 0; i < PlayerInformation.GetLength(0); i++)
            {
                if (PlayerInformation[i, 3] == yes)
                {
                    string statusOnline = PlayerInformation[i, 1];
                    if (statusOnline == "y")
                    {
                        statusOnline = "Online";
                    }
                    else
                    {
                        statusOnline = "Offline";
                    }

                    string inGame = PlayerInformation[i, 2];
                    if (inGame == "n")
                    {
                        inGame = "NotBusy";
                    }
                    else
                    {
                        inGame = "InGame";
                    }

                    if (statusOnline != "Online")
                    {
                        FriendsList.Items.Add(PlayerInformation[i, 0] + "(" + statusOnline + ")");

                    }
                    else
                    {
                        FriendsList.Items.Add(PlayerInformation[i, 0] + "(" + statusOnline + "," + inGame + ")");
                    }
                }
            }
            FriendsList.Sorted = true;
        }
        public void FillOthersList()
        {
            string yes = "y";
            string no = "n";
            for (int i = 0; i < PlayerInformation.GetLength(0); i++)
            {
                if (PlayerInformation[i, 1] == yes && PlayerInformation[i, 3] == no)
                {
                    string statusOnline = PlayerInformation[i, 1];
                    if (statusOnline == "y")
                    {
                        statusOnline = "Online";
                    }
                    else
                    {
                        statusOnline = "Offline";
                    }

                    string inGame = PlayerInformation[i, 2];
                    if (inGame == "n")
                    {
                        inGame = "NotBusy";
                    }
                    else
                    {
                        inGame = "InGame";
                    }

                    OthersList.Items.Add(PlayerInformation[i, 0] + "(" + statusOnline + "," + inGame + ")");

                }
            }
            OthersList.Sorted = true;
        }
        public void UpdateFriendAndOthersList(string username)
        {
            // PlayerInformation = _connect.checkOnline(username);
            PlayerInformation = new string[,]{ { "Brookly", "y", "n", "y"},
                                  { "Jathan", "n", "n", "y"},
                                  { "Joseph", "y", "y", "n"},
                                  { "Brandon", "y", "n", "n"},
                                  { "Darien", "y", "n", "y" },
                                  { "Steven", "y", "y", "n" }};
            FillFriendsList();
            FillOthersList();
        }

        private void banUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BanUserWindow ban = new BanUserWindow(Username, Password);
            ban.Show();
        }

        private void deleteUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminDeleteWindow aDelete = new AdminDeleteWindow(Username, Password);
            aDelete.Show();
        }

        private void resetUserPasswordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AdminWindow adminReset = new AdminWindow(Username, Password);
            adminReset.ShowDialog();
        }
    }
}
