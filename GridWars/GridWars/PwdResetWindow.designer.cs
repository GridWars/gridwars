﻿namespace GridWars
{
    partial class PwdResetWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.emailLabel = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.resetPrompt = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pwdResetSendButton = new System.Windows.Forms.Button();
            this.pwdResetBackButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // emailLabel
            // 
            this.emailLabel.AutoSize = true;
            this.emailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.emailLabel.Location = new System.Drawing.Point(70, 176);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(52, 20);
            this.emailLabel.TabIndex = 4;
            this.emailLabel.Text = "Email:";
            this.emailLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(128, 178);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(208, 20);
            this.emailTextBox.TabIndex = 5;
            // 
            // resetPrompt
            // 
            this.resetPrompt.Location = new System.Drawing.Point(12, 75);
            this.resetPrompt.Name = "resetPrompt";
            this.resetPrompt.Size = new System.Drawing.Size(412, 47);
            this.resetPrompt.TabIndex = 6;
            this.resetPrompt.Text = "Please enter your account\'s email below. Once verified, an email containing a tem" +
    "porary password will be sent to the address. This password can be used the next " +
    "time you log into your account.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(12, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Password Reset";
            // 
            // pwdResetSendButton
            // 
            this.pwdResetSendButton.Location = new System.Drawing.Point(226, 245);
            this.pwdResetSendButton.Name = "pwdResetSendButton";
            this.pwdResetSendButton.Size = new System.Drawing.Size(75, 23);
            this.pwdResetSendButton.TabIndex = 8;
            this.pwdResetSendButton.Text = "Send";
            this.pwdResetSendButton.UseVisualStyleBackColor = true;
            this.pwdResetSendButton.Click += new System.EventHandler(this.pwdResetSendButton_Click);
            // 
            // pwdResetBackButton
            // 
            this.pwdResetBackButton.Location = new System.Drawing.Point(127, 245);
            this.pwdResetBackButton.Name = "pwdResetBackButton";
            this.pwdResetBackButton.Size = new System.Drawing.Size(75, 23);
            this.pwdResetBackButton.TabIndex = 9;
            this.pwdResetBackButton.Text = "Back";
            this.pwdResetBackButton.UseVisualStyleBackColor = true;
            this.pwdResetBackButton.Click += new System.EventHandler(this.pwdResetBackButton_Click);
            // 
            // PwdResetWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 331);
            this.Controls.Add(this.pwdResetBackButton);
            this.Controls.Add(this.pwdResetSendButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.resetPrompt);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.emailLabel);
            this.Name = "PwdResetWindow";
            this.Text = "Password Reset";
            this.Load += new System.EventHandler(this.PwdResetWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label resetPrompt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button pwdResetSendButton;
        private System.Windows.Forms.Button pwdResetBackButton;
    }
}