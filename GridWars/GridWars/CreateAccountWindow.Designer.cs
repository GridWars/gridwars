﻿namespace GridWars
{
    partial class CreateAccountWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.ConfirmPasstxtBox = new System.Windows.Forms.TextBox();
            this.PasswordtxtBox = new System.Windows.Forms.TextBox();
            this.EmailtxtBox = new System.Windows.Forms.TextBox();
            this.UserNametxtBox = new System.Windows.Forms.TextBox();
            this.ScreenNametxtBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.UserNamelbl = new System.Windows.Forms.Label();
            this.ScreenNamelbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(76, 242);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(106, 26);
            this.btnCancel.TabIndex = 23;
            this.btnCancel.Text = "Go Back ";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(221, 242);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(106, 26);
            this.btnCreate.TabIndex = 22;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // ConfirmPasstxtBox
            // 
            this.ConfirmPasstxtBox.Location = new System.Drawing.Point(111, 181);
            this.ConfirmPasstxtBox.Name = "ConfirmPasstxtBox";
            this.ConfirmPasstxtBox.Size = new System.Drawing.Size(181, 20);
            this.ConfirmPasstxtBox.TabIndex = 21;
            // 
            // PasswordtxtBox
            // 
            this.PasswordtxtBox.Location = new System.Drawing.Point(111, 156);
            this.PasswordtxtBox.Name = "PasswordtxtBox";
            this.PasswordtxtBox.Size = new System.Drawing.Size(181, 20);
            this.PasswordtxtBox.TabIndex = 20;
            // 
            // EmailtxtBox
            // 
            this.EmailtxtBox.Location = new System.Drawing.Point(111, 128);
            this.EmailtxtBox.Name = "EmailtxtBox";
            this.EmailtxtBox.Size = new System.Drawing.Size(181, 20);
            this.EmailtxtBox.TabIndex = 19;
            // 
            // UserNametxtBox
            // 
            this.UserNametxtBox.Location = new System.Drawing.Point(111, 100);
            this.UserNametxtBox.Name = "UserNametxtBox";
            this.UserNametxtBox.Size = new System.Drawing.Size(181, 20);
            this.UserNametxtBox.TabIndex = 18;
            // 
            // ScreenNametxtBox
            // 
            this.ScreenNametxtBox.Location = new System.Drawing.Point(111, 74);
            this.ScreenNametxtBox.Name = "ScreenNametxtBox";
            this.ScreenNametxtBox.Size = new System.Drawing.Size(181, 20);
            this.ScreenNametxtBox.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 185);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Confirm Password: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(55, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Password: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(73, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "E-mail: ";
            // 
            // UserNamelbl
            // 
            this.UserNamelbl.AutoSize = true;
            this.UserNamelbl.Location = new System.Drawing.Point(48, 103);
            this.UserNamelbl.Name = "UserNamelbl";
            this.UserNamelbl.Size = new System.Drawing.Size(66, 13);
            this.UserNamelbl.TabIndex = 13;
            this.UserNamelbl.Text = "User Name: ";
            // 
            // ScreenNamelbl
            // 
            this.ScreenNamelbl.AutoSize = true;
            this.ScreenNamelbl.Location = new System.Drawing.Point(36, 76);
            this.ScreenNamelbl.Name = "ScreenNamelbl";
            this.ScreenNamelbl.Size = new System.Drawing.Size(78, 13);
            this.ScreenNamelbl.TabIndex = 12;
            this.ScreenNamelbl.Text = "Screen Name: ";
            // 
            // CreateAccountWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(365, 298);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.ConfirmPasstxtBox);
            this.Controls.Add(this.PasswordtxtBox);
            this.Controls.Add(this.EmailtxtBox);
            this.Controls.Add(this.UserNametxtBox);
            this.Controls.Add(this.ScreenNametxtBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.UserNamelbl);
            this.Controls.Add(this.ScreenNamelbl);
            this.Name = "CreateAccountWindow";
            this.Text = "Create Account";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.TextBox ConfirmPasstxtBox;
        private System.Windows.Forms.TextBox PasswordtxtBox;
        private System.Windows.Forms.TextBox EmailtxtBox;
        private System.Windows.Forms.TextBox UserNametxtBox;
        private System.Windows.Forms.TextBox ScreenNametxtBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label UserNamelbl;
        private System.Windows.Forms.Label ScreenNamelbl;
    }
}