﻿namespace GridWars
{
    partial class BannedWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BannedMessage = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // BannedMessage
            // 
            this.BannedMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BannedMessage.Location = new System.Drawing.Point(12, 21);
            this.BannedMessage.Multiline = true;
            this.BannedMessage.Name = "BannedMessage";
            this.BannedMessage.ReadOnly = true;
            this.BannedMessage.Size = new System.Drawing.Size(371, 158);
            this.BannedMessage.TabIndex = 0;
            this.BannedMessage.Text = "Alert:\r\n\r\nYou have been banned. During this period you are unable to play or comm" +
    "unicate with other players. Please speak with an administrator to get this resol" +
    "ved. ";
            this.BannedMessage.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // BannedScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 191);
            this.Controls.Add(this.BannedMessage);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BannedScreen";
            this.ShowIcon = false;
            this.Text = "Banned";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.BannedScreen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox BannedMessage;
    }
}