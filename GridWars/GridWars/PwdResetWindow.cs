﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GridWars;

namespace GridWars
{
    public partial class PwdResetWindow : Form
    {
        public PwdResetWindow()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pwdResetBackButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void pwdResetSendButton_Click(object sender, EventArgs e)
        {

            //email send

            string tempPassword = GridWars.Program.GenerateRandomString(10);

            MessageBox.Show("An email containing your temporary password has been sent to " + emailTextBox.Text + ".");
            this.Hide();
           
        }

        private void PwdResetWindow_Load(object sender, EventArgs e)
        {

        }
    }
}
