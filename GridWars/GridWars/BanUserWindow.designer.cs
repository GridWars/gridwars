﻿namespace GridWars
{
    partial class BanUserWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ToBanUserList = new System.Windows.Forms.ListBox();
            this.Ban = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.BanInstructions = new System.Windows.Forms.TextBox();
            this.SelectedUsername = new System.Windows.Forms.TextBox();
            this.SelectedUserMessage = new System.Windows.Forms.TextBox();
            this.UserSelectError = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // ToBanUserList
            // 
            this.ToBanUserList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.ToBanUserList.FormattingEnabled = true;
            this.ToBanUserList.ItemHeight = 20;
            this.ToBanUserList.Location = new System.Drawing.Point(284, 33);
            this.ToBanUserList.Name = "ToBanUserList";
            this.ToBanUserList.Size = new System.Drawing.Size(210, 344);
            this.ToBanUserList.TabIndex = 2;
            this.ToBanUserList.SelectedIndexChanged += new System.EventHandler(this.ToBanUserList_SelectedIndexChanged);
            // 
            // Ban
            // 
            this.Ban.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Ban.Location = new System.Drawing.Point(86, 454);
            this.Ban.Name = "Ban";
            this.Ban.Size = new System.Drawing.Size(110, 40);
            this.Ban.TabIndex = 3;
            this.Ban.Text = "Ban";
            this.Ban.UseVisualStyleBackColor = true;
            this.Ban.Click += new System.EventHandler(this.Ban_Click);
            // 
            // Cancel
            // 
            this.Cancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Cancel.Location = new System.Drawing.Point(284, 454);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(110, 40);
            this.Cancel.TabIndex = 4;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // BanInstructions
            // 
            this.BanInstructions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BanInstructions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.BanInstructions.Location = new System.Drawing.Point(24, 49);
            this.BanInstructions.Multiline = true;
            this.BanInstructions.Name = "BanInstructions";
            this.BanInstructions.ReadOnly = true;
            this.BanInstructions.Size = new System.Drawing.Size(226, 67);
            this.BanInstructions.TabIndex = 5;
            this.BanInstructions.Text = "Select a username from the list on the right to ban.";
            // 
            // SelectedUsername
            // 
            this.SelectedUsername.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SelectedUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.SelectedUsername.Location = new System.Drawing.Point(24, 198);
            this.SelectedUsername.Name = "SelectedUsername";
            this.SelectedUsername.ReadOnly = true;
            this.SelectedUsername.ShortcutsEnabled = false;
            this.SelectedUsername.Size = new System.Drawing.Size(226, 19);
            this.SelectedUsername.TabIndex = 6;
            this.SelectedUsername.TabStop = false;
            // 
            // SelectedUserMessage
            // 
            this.SelectedUserMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SelectedUserMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.SelectedUserMessage.Location = new System.Drawing.Point(24, 173);
            this.SelectedUserMessage.Name = "SelectedUserMessage";
            this.SelectedUserMessage.ReadOnly = true;
            this.SelectedUserMessage.Size = new System.Drawing.Size(226, 19);
            this.SelectedUserMessage.TabIndex = 7;
            this.SelectedUserMessage.Text = "Selected username:";
            // 
            // UserSelectError
            // 
            this.UserSelectError.BackColor = System.Drawing.SystemColors.Menu;
            this.UserSelectError.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UserSelectError.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.UserSelectError.ForeColor = System.Drawing.Color.Red;
            this.UserSelectError.Location = new System.Drawing.Point(24, 334);
            this.UserSelectError.Multiline = true;
            this.UserSelectError.Name = "UserSelectError";
            this.UserSelectError.ReadOnly = true;
            this.UserSelectError.Size = new System.Drawing.Size(254, 43);
            this.UserSelectError.TabIndex = 8;
            this.UserSelectError.Text = "Error! Please select a username from the list to the right";
            this.UserSelectError.Visible = false;
            // 
            // BanUserWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 525);
            this.Controls.Add(this.UserSelectError);
            this.Controls.Add(this.SelectedUserMessage);
            this.Controls.Add(this.SelectedUsername);
            this.Controls.Add(this.BanInstructions);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.Ban);
            this.Controls.Add(this.ToBanUserList);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BanUserWindow";
            this.ShowIcon = false;
            this.Text = "BanUserScreen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion
        private System.Windows.Forms.ListBox ToBanUserList;
        private System.Windows.Forms.Button Ban;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.TextBox BanInstructions;
        private System.Windows.Forms.TextBox SelectedUsername;
        private System.Windows.Forms.TextBox SelectedUserMessage;
        private System.Windows.Forms.TextBox UserSelectError;
    }
}