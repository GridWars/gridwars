﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using dataconn;

namespace GridWars
{
    public partial class BanUserWindow : Form
    {
        public string Username = "";
        private dbconn _connect;
        public string[,] PlayerInformation;

        public BanUserWindow(string username, string password)
        {
            InitializeComponent();
            Username = username;
            _connect = new dbconn(username, password);

            updateUserList(username);

        }

        public void updateUserList(string username)
        {
            PlayerInformation = _connect.checkOnline(username);

            for (int i = 0; i < (PlayerInformation.Length / 4); i++)
            {
                ToBanUserList.Items.Add(PlayerInformation[i, 0]);

            }

        }

        private void Ban_Click(object sender, EventArgs e)
        {
            try {
                ToBanUserList.SelectedItem.ToString();          //test if a user was selected
            }
            catch (System.NullReferenceException n) {
                UserSelectError.Visible = true;                 //if not display error message
                return;
            }

            string acceptBan = "Are you sure you wish to ban " + ToBanUserList.SelectedItem.ToString();
            string title = "Ban?";
            DialogResult result = MessageBox.Show(this, acceptBan, title, MessageBoxButtons.YesNo);     //display validation of ban selection
            if(result == DialogResult.Yes){
                //insert code to ban
                string success = ToBanUserList.SelectedItem.ToString() + " has been successfully banned.";
                MessageBox.Show(this, success, title, MessageBoxButtons.OK);                 //display message of successful ban
                Close();
            }
            else{
                ToBanUserList.ClearSelected();                              //clear selected user and allow selection of different user
            }
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();                                                        //close the ban window
        }

        private void ToBanUserList_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserSelectError.Visible = false;
            try{
                SelectedUsername.Text = ToBanUserList.SelectedItem.ToString();      //test if a user is selected, if so display that name
            }
            catch(Exception ex){
                SelectedUsername.Text = "";                                         //if no user is selected, display nothing
            }
        }
    }
}
