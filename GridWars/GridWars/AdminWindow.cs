﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using dataconn;

namespace GridWars
{
    public partial class AdminWindow : Form
    {
        public string Username = "";
        private dbconn _connect;
        public string[,] PlayerInformation;

        public AdminWindow (string username, string password)
        {
            InitializeComponent ( );
            Username = username;
            _connect = new dbconn(username, password);
      
            ShowUsers (username);
        }

        private void listBox1_SelectedIndexChanged (object sender, EventArgs e)
        {

        }

        private void label1_Click (object sender, EventArgs e)
        {

        }

        private void ShowUsers (string username)
        {
            PlayerInformation = _connect.checkOnline(username);

            for (int i = 0; i < (PlayerInformation.Length / 4); i++)
            {
                listBox1.Items.Add(PlayerInformation[i, 0]);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void AdminWindow_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }
    }
}
