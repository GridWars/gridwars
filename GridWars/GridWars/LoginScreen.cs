﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using dataconn;
using Npgsql;

namespace GridWars
{
    public partial class LoginScreen : Form
    {
        public LoginScreen()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            ValidateUsernameAndPassword();
        }

        public void ValidateUsernameAndPassword()
        {
            string username = usernameField.Text;
            string password = passwordField.Text;

            Boolean loggedon = false;

            try
            {
                dbconn _connect = new dbconn(username, password);

                loggedon = _connect.login(username);
                Boolean verifiedEmail =_connect.verifyEmail(username);

                if (loggedon)
                {
                    if (verifiedEmail == true)
                    {
                        this.Hide();
                        GamePlayWindow mainGameWindow = new GamePlayWindow(username, password);
                        mainGameWindow.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Please verify your email address. ");

                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show("Username or Password is incorrect. Please try again. ");
            }
            
                
        }

        private void username_TextChanged(object sender, EventArgs e)
        {

        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CreateAccountWindow createAccount = new CreateAccountWindow();
            createAccount.ShowDialog();
        }

        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            PwdResetWindow loginPwdResetWindow = new PwdResetWindow();
            loginPwdResetWindow.ShowDialog();
        }
    }
}
